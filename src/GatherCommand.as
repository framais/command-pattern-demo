package  
{
	import com.greensock.TweenMax;
	import flash.events.Event;

	public class GatherCommand extends AsyncCommand implements IAsyncCommand
	{
		public var vehicle:Vehicle;
		
		public function GatherCommand(vehicle:Vehicle) 
		{
			this.vehicle = vehicle;
		}
		
		override public function execute():void 
		{
			var gatherTween:TweenMax = vehicle.gather();
			gatherTween.addEventListener(Event.COMPLETE, onComplete);
		}
		
		private function onComplete(e:Event):void 
		{
			complete();
		}		
	}
}