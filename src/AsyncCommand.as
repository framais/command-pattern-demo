package  
{
	public class AsyncCommand implements IAsyncCommand
	{
		public var callback:Function;
		
		public function execute():void 
		{
			
		}
		
		public function addCompleteCallback(callback:Function):void
		{
			this.callback = callback;
		}
		
		public function complete():void
		{
			callback.call();
		}
	}
}