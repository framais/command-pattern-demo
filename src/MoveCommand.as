package  
{
	import com.greensock.TweenMax;
	import flash.events.Event;

	public class MoveCommand extends AsyncCommand implements IAsyncCommand
	{
		public var vehicle:Vehicle;
		private var targetX:Number;
		private var targetY:Number;
		
		public function MoveCommand(vehicle:Vehicle, targetX:Number, targetY:Number) 
		{
			this.vehicle = vehicle;
			this.targetX = targetX;
			this.targetY = targetY;
		}
		
		override public function execute():void 
		{
			var moveTween:TweenMax = vehicle.move(targetX, targetY);
			moveTween.addEventListener(Event.COMPLETE, onComplete);
		}
		
		private function onComplete(e:Event):void 
		{
			complete();
		}			
	}
}