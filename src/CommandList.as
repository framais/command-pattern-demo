package  
{
	
	public class CommandList 
	{
		private var commandsStack:Array = [ ];
		private var isBusy:Boolean = false;		
		
		public function add(command:IAsyncCommand):void 
		{
			command.addCompleteCallback(executeNext);
			commandsStack.push(command);
			attemptExecute();
		}
		
		private function attemptExecute():void
		{
			if (!isBusy)
			{
				executeNext();
			}
		}
		
		private function executeNext():void
		{
			isBusy = false;
			if (commandsStack.length > 0)
			{
				isBusy = true;
				var command:IAsyncCommand = commandsStack.shift() as IAsyncCommand;
				command.execute();
			}
		}
	}
}