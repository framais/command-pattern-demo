package  
{
	public interface IAsyncCommand 
	{		
		function complete():void;
		function addCompleteCallback(callback:Function):void;
		function execute():void;
	}	
}