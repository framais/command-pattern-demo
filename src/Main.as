package 
{
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	[SWF(backgroundColor = "0xededed", frameRate = "60")]
	
	public class Main extends Sprite 
	{
		private var vehicle:Vehicle;	
		private var commandList:CommandList;
		
		public function Main():void 
		{
			vehicle = new Vehicle();
			addChild(vehicle);
			
			commandList = new CommandList();
			
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			stage.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			commandList.add(new MoveCommand(vehicle, e.stageX, e.stageY));
		}
		
		private function onKeyUp(e:KeyboardEvent):void 
		{
			switch (e.keyCode)
			{
				case 49: // Keyboard 1
					commandList.add(new AttackCommand(vehicle));
					break;
					
				case 50: // Keyboard 2
					commandList.add(new GatherCommand(vehicle));
					break;
			}
		}		
	}	
}