package  
{
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	
	public class Vehicle extends Sprite 
	{
		public function Vehicle() 
		{			
			graphics.beginFill(0x0000cc);
			graphics.drawRect( -15, -15, 30, 30);
			graphics.endFill();
		}
		
		public function attack():TweenMax
		{
			trace("attacking");
			var attackTween:TweenMax = TweenMax.to(this, 0.4, { rotation: this.rotation + 90 } );
			return attackTween;
		}
		
		public function gather():TweenMax
		{
			trace("gathering");
			var gatherTween:TweenMax = new TweenMax(this, 0.3, { tint:0x22dd00, onComplete:function():void{gatherTween.reverse()}} );			
			return gatherTween;
		}
		
		public function move(targetX:Number, targetY:Number):TweenMax
		{
			trace("moving");
			var moveTween:TweenMax = TweenMax.to(this, 1, { x:targetX, y:targetY } );
			return moveTween;
		}
	}
}