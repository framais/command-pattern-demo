package  
{
	import com.greensock.TweenMax;
	import flash.events.Event;

	public class AttackCommand extends AsyncCommand implements IAsyncCommand
	{
		public var vehicle:Vehicle;
		
		public function AttackCommand(vehicle:Vehicle) 
		{
			this.vehicle = vehicle;
		}
		
		override public function execute():void 
		{
			var attackTween:TweenMax = vehicle.attack();
			attackTween.addEventListener(Event.COMPLETE, onComplete);
		}
		
		private function onComplete(e:Event):void 
		{
			complete();
		}		
	}
}